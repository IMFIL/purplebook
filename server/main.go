package main

import (
	"fmt"
	"net/http"
	jsoniter "github.com/json-iterator/go"
	"gopkg.in/go-playground/validator.v9"
	"github.com/jinzhu/gorm"
  	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var json = jsoniter.ConfigCompatibleWithStandardLibrary
// use a single instance of Validate, it caches struct info
var validate *validator.Validate

// HTTPHandler is the struct holding shared state for HTTP handlers
type HTTPHandler struct {}

var db *gorm.DB

func main() {
	validate = validator.New()
	DoHTTP()
}

// DoHTTP will start the HTTP server
func DoHTTP() {
	var err error
	// set those to be env vars for production ready products
	db, err = gorm.Open("postgres", "host=localhost port=5432 user=postgres dbname=purplebook password=postgres sslmode=disable")
	if err != nil {
		fmt.Println(err)
		return
	}
  	defer db.Close()		
	handler := HTTPHandler{}
	mux := http.NewServeMux()
	mux.HandleFunc("/account/create", handler.CreateAccountHandler)
	mux.HandleFunc("/sign_in", handler.SignInHandler)

	// log that you are listening 

	// read from env var
	server := http.Server{Handler: mux, Addr: ":5000"}

	err = server.ListenAndServe()
	if err != nil {
		// panic
	}
}

