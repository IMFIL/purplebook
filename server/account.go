package main

import (
	"fmt"
	"net/http"
	"io/ioutil"
	"github.com/gofrs/uuid"
	"time"
)

// User is the struct that represents a create account object
type User struct {
	ID uuid.UUID `json:"id"`
	CreatedAt time.Time `json:"created_at"`
	IsAdmin bool `json:"is_admin"`
	Name string `json:"name" validate:"required"`
	Email string `json:"email" validate:"required"`
	Password string `json:"pw" validate:"required"`
}


// CreateAccountHandler handles all account related requests
func (h *HTTPHandler) CreateAccountHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	// remove get after testing
	w.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type,access-control-allow-origin, access-control-allow-headers")

	// if the browser is sending an OPTIONS
	// request we simply return as we do not want
	// to validate anything
	if r.Method == "OPTIONS" {
		return
	}

	fullBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		// log error
		http.Error(w, "error reading body", 500)

		return
	}

	var user User

	err = json.Unmarshal(fullBody, &user)
	if err != nil {
		// log error
		http.Error(w, "error reading body", 500)
		return
	}

	err = validate.Struct(user)
	if err != nil {
		// log error
		http.Error(w, "error reading body, missing or invalid data", 400)
		return
	}

	// Create a Version 4 UUID.
	u, err := uuid.NewV4()
	if err != nil {
		// log error
		http.Error(w, "server error", 500)
		return
	}

	user.ID = u
	user.CreatedAt = time.Now()
	user.IsAdmin = false

	// check if email exists
	// creating the record
	if !db.NewRecord(&user) {
		fmt.Println(db.NewRecord(&user))
		http.Error(w, "user could not be created", 500)
		return
	}
	// saving to the db
	db.Create(&user)

	fmt.Fprintf(w, "ok")
}
