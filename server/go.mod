module purplebook

go 1.13

require (
	github.com/corpix/uarand v0.1.1 // indirect
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/icrowley/fake v0.0.0-20180203215853-4178557ae428
	github.com/jinzhu/gorm v1.9.12
	github.com/json-iterator/go v1.1.9
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/stretchr/testify v1.5.1
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/validator.v9 v9.31.0
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
