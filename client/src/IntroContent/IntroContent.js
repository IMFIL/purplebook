import React, { Component } from "react";
import "./IntroContent.css";
import { Button, Image, Heading } from "grommet";
import polygon_green from "../res/images/polygon_green.svg";
import polygon_white from "../res/images/polygon_white.svg";

class IntroContent extends Component {
  constructor(props) {
    super(props);

    this.onRegisterClick = this.onRegisterClick.bind(this);
    this.onSigninClick = this.onSigninClick.bind(this);
  }

  onRegisterClick() {
    this.props.history.push("/register");
  }

  onSigninClick() {
    this.props.history.push("/signin");
  }

  render() {
    return (
      <div className="introContent">
        <Image className="polygon_green" fit="cover" src={polygon_green} />
        <Image className="polygon_white" fit="cover" src={polygon_white} />
        <Heading
          className="introTitle"
          alignSelf="center"
          size="medium"
          color="white"
          margin="medium"
        >
          Read and create amazing content.
        </Heading>
        <div className="accountSetup">
          <Button
            margin={"small"}
            color={"#6FFEB0"}
            label="Register"
            onClick={this.onRegisterClick}
          />
          <Button
            color={"#6FFEB0"}
            margin={"small"}
            label="Sign in"
            onClick={this.onSigninClick}
          />
        </div>
      </div>
    );
  }
}

export default IntroContent;
