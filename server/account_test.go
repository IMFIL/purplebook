package main

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

func TestUniqueEmail(t *testing.T)  {
	user, err := getBaseUser()
	assert.NoError(t, err)
	// check if email exists
	// creating the record
	db.NewRecord(&user)
	// saving to the db
	db.Create(&user)

	user2, err := getBaseUser()
	assert.NoError(t, err)
	assert.False(t, db.NewRecord(&user2))

}

func TestUniqueID(t *testing.T)  {
	user, err := getBaseUser()
	assert.NoError(t, err)
	// check if email exists
	// creating the record
	db.NewRecord(&user)
	// saving to the db
	db.Create(&user)

	user2, err := getBaseUser()
	assert.NoError(t, err)
	user2.ID = user.ID
	assert.False(t, db.NewRecord(&user2))
}
