# purplebook

**Team**

Filip Slatinac; 8273487



**Short Description**

Purple book is a blogging and article sharing tool.

## UI Design

**Mock ups**

![alt text](https://gitlab.com/IMFIL/purplebook/-/raw/master/client/HTMLMocks/ui-design-images/frontPage.png)
Front page

![alt text](https://gitlab.com/IMFIL/purplebook/-/raw/master/client/HTMLMocks/ui-design-images/signinPage.png)
Sign-in page

![alt text](https://gitlab.com/IMFIL/purplebook/-/raw/master/client/HTMLMocks/ui-design-images/registerPage.png)
Register page

![alt text](https://gitlab.com/IMFIL/purplebook/-/raw/master/client/HTMLMocks/ui-design-images/activityFeedPage.png)
Activity-Feed page

The html and css used to create these pages can be found in HTMLmocks. The structure of those pages can be found in the src folder in their respective components.
To see this app in action you have to do the following steps on a linx machine in the directory of the project:

```
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew install node
brew upgrade node
npm install
npm start
```

Then go to localhost:3000 and the website should be there

**Colour palette**

![alt text](https://gitlab.com/IMFIL/purplebook/-/raw/master/client/HTMLMocks/ui-design-images/main.png)

Primary colour

![alt text](https://gitlab.com/IMFIL/purplebook/-/raw/master/client/HTMLMocks/ui-design-images/secondary.png)

Secondary colour

![alt text](https://gitlab.com/IMFIL/purplebook/-/raw/master/client/HTMLMocks/ui-design-images/complimentary.png)

Complimentary colour

**Fonts and type scale**

Base font size: 6 pt
Maximum font size: 48 pt

Increase from minumum to maximum by a multiplication factor of 2, thus resulting in the folowing scale: *6 pt, 12 pt, 24 pt, 48 pt*

Main font family: *Montserrat*

Secondary font family: *Roboto*

**UI Components**

![alt text](https://gitlab.com/IMFIL/purplebook/-/raw/master/client/HTMLMocks/ui-design-images/button.png)

Buttons

![alt text](https://gitlab.com/IMFIL/purplebook/-/raw/master/client/HTMLMocks/ui-design-images/formInput.png)

Input boxes

![alt text](https://gitlab.com/IMFIL/purplebook/-/raw/master/client/HTMLMocks/ui-design-images/textLink.png)

Text Links

![alt text](https://gitlab.com/IMFIL/purplebook/-/raw/master/client/HTMLMocks/ui-design-images/modularBox.png)

Modular boxeslogy**

For my server technology i decided to use Golang as I wanted to learn the language. In addition to my desires to learn, golang's verbosity and ease of use made it more appealing as a language. The web app written in Golang will be deployed to Google cloud run container which means that it will be able to scale to as many instances needed without me having to intervene in it's operation.



**Database Technology**

PostgreSQL will be the database technology used in this project.



**Automated Tests**

Tests can be found under the /server repository and can be ran with the command`go test -v` in the /server repository. Also, I have setup automated testing on every push to master. That configuration can be found in /gitlab-ci.yml. 



**Refined UI and  Web App Interactivity**

The web app can be interacted with by following the commands listed below.



**Deployment and usage**

`cd /purplebook`

`cd client`

`npm install && npm run start`

`docker-compose up -d`

`cd server`

`./scripts/makedb.sh`

`go build && go ./purplebook`



You will be able to create an account under localhost:3000.

