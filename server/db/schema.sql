CREATE TABLE sessions (
        id uuid NOT NULL,
        user_id uuid NOT NULL,
        created_at date NOT NULL,
        expires_at date NOT NULL
);

CREATE TABLE expired_sessions (
        id uuid NOT NULL
);

CREATE TABLE users (
        id uuid NOT NULL UNIQUE,
        name text NOT NULL, 
        email text NOT NULL UNIQUE, 
        password varchar NOT NULL, 
        is_admin boolean NOT NULL, 
        created_at date NOT NULL
);
