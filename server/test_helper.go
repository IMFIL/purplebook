package main

import (
	"time"
	"github.com/gofrs/uuid"
	"github.com/icrowley/fake"
	"github.com/jinzhu/gorm"
)

	// IsAdmin bool `json:"is_admin"`
	// Name string `json:"name" validate:"required"`
	// Email string `json:"email" validate:"required"`
	// Password string `json:"pw" validate:"required"`

func getBaseUser() (*User, error) {
	// Create a Version 4 UUID.
	u, err := uuid.NewV4()
	if err != nil {
		// log error
		return nil, err
	}	
	baseU := User{
		ID: u,
		CreatedAt: time.Now(),
		Password: "123123",
		Email: fake.EmailAddress(),
		Name: "johnny cash",
		IsAdmin: false,
	}
	return &baseU, nil
}

func testInit() {
	db, _ = gorm.Open("postgres", "host=localhost port=5432 user=postgres dbname=purplebook password=postgres sslmode=disable")
}
