import React, { Component } from "react";
import IntroContent from "../IntroContent/IntroContent";
import Register from "../Register/Register";
import Signin from "../Signin/Signin";
import ArticleFeed from "../ArticleFeed/ArticleFeed";
import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Grommet, Footer, Anchor, Text, Header, Main } from "grommet";
const myTheme = {
  global: {
    font: {
      family: "Montserrat",
    },
  },
  button: {
    color: "white",
  },
  formField: {
    label: {
      color: "white",
    },
    border: {
      color: "rgb(111, 254, 176)",
    },
  },
};

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <Grommet full={true} theme={myTheme}>
            <Header background="brand">
              <Text margin={"small"}>Purplebook</Text>
            </Header>
            <Main
              className="mainContainer"
              align="center"
              justify="center"
              fill={true}
            >
              <Switch>
                <Route exact path="/" component={IntroContent} />
                <Route exact path="/register" component={Register} />
                <Route exact path="/signin" component={Signin} />
                <Route exact path="/feed" component={ArticleFeed} />
              </Switch>
            </Main>
            <Footer background="brand" pad="medium">
              <Text>Copyright</Text>
              <Anchor label="About" />
            </Footer>
          </Grommet>
        </div>
      </Router>
    );
  }
}

export default App;
