package main

import (
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	testInit()
	exitCode := m.Run()
	os.Exit(exitCode)
}
