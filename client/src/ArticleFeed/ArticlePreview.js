import React, { Component } from "react";
import { Box, Image, Text, Paragraph } from "grommet";

class ArticlePreview extends Component {
  render() {
    return (
      <Box
        margin="medium"
        elevation="medium"
        direction="row"
        alignSelf="center"
        pad="large"
        round
        background={{ color: "light-2", opacity: "strong" }}
      >
        <Box margin={{ right: "medium" }}>
          <Text size="small" color="grey">
            Startups
          </Text>
          <Text color="black">What is like running a startup in 2020?</Text>
          <Box width="medium">
            <Text wordBreak="break-word" size="xsmall" color="grey">
              Dive deep into the experiences of modern day entrepreneurs that
              are trying to change the world.
            </Text>
          </Box>
          <Text margin={{ top: "medium" }} size="xsmall" color="grey">
            Feb 6 2020
          </Text>
        </Box>

        <Box height="small" width="small">
          <Image src="//v2.grommet.io/assets/IMG_4245.jpg" fit="cover" />
        </Box>
      </Box>
    );
  }
}

export default ArticlePreview;
