package main

import (
	"fmt"
	"net/http"
	"io/ioutil"
)

// SignInObject is the struct that represents a sign in object
type SignInObject struct {
	Email string `json:"email" validate:"required"`
	Password string `json:"pw" validate:"required"`
}


// SignInHandler handles sign in
func (h *HTTPHandler) SignInHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	// remove get after testing
	w.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type,access-control-allow-origin, access-control-allow-headers")

	// if the browser is sending an OPTIONS
	// request we simply return as we do not want
	// to validate anything
	if r.Method == "OPTIONS" {
		return
	}

	fullBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		// log error
		http.Error(w, "error reading body", 500)

		return
	}

	var creds SignInObject

	err = json.Unmarshal(fullBody, &creds)
	if err != nil {
		// log error
		http.Error(w, "error reading body", 500)
		return
	}

	err = validate.Struct(creds)
	if err != nil {
		// log error
		http.Error(w, "error reading body, missing or invalid data", 400)
		return
	}

	// check if pw matches stored password for that email
	// create a session 
	// return the session

	fmt.Printf("%v \n", creds)

	fmt.Fprintf(w, "ok")
}
