import axios from "axios";
import React, { Component } from "react";
import { Form, FormField, Button, Box, Heading, Image } from "grommet";
import polygon_green from "../res/images/polygon_green.svg";

class Signin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      pw: "",
    };
  }

  onEmailChange(e) {
    this.setState({
      email: e.target.value,
    });
  }

  onPWChange(e) {
    this.setState({
      pw: e.target.value,
    });
  }

  onSubmitClick() {
    // put this url in constants somewhere and load
    // localhost if on localhost else load the real api URL
    axios
      .post("http://localhost:5000/sign_in", {
        email: this.state.email,
        pw: this.state.pw,
      })
      .then(response => {
        this.props.history.push("/feed");
      });
  }
  render() {
    return (
      <Box width="medium">
        <Image
          className="polygon_green_signin"
          fit="cover"
          src={polygon_green}
        />
        <Heading
          className="introTitle"
          alignSelf="center"
          size="small"
          color="white"
          margin="medium"
        >
          Welcome back
        </Heading>
        <Form>
          <FormField
            onChange={this.onEmailChange.bind(this)}
            margin="medium"
            name="email"
            label="Email"
          />
          <FormField
            onChange={this.onPWChange.bind(this)}
            margin="medium"
            type="password"
            name="password"
            label="Password"
          />
          <Button
            onClick={this.onSubmitClick.bind(this)}
            margin={"small"}
            color={"#6FFEB0"}
            type="submit"
            label="Submit"
          />
        </Form>
      </Box>
    );
  }
}

export default Signin;
