import axios from "axios";
import React, { Component } from "react";
import { Form, FormField, Button, Box, Heading, Image } from "grommet";
import polygon_white from "../res/images/polygon_white.svg";

class Register extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "",
      email: "",
      pw: "",
    };
  }

  onNameChange(e) {
    this.setState({
      name: e.target.value,
    });
  }

  onEmailChange(e) {
    this.setState({
      email: e.target.value,
    });
  }

  onPWChange(e) {
    this.setState({
      pw: e.target.value,
    });
  }

  onSubmitClick() {
    // put this url in constants somewhere and load
    // localhost if on localhost else load the real api URL
    axios
      .post("http://localhost:5000/account/create", {
        name: this.state.name,
        email: this.state.email,
        pw: this.state.pw,
      })
      .then(response => {
        this.props.history.push("/feed");
      });
  }

  render() {
    return (
      <Box width="medium">
        <Image
          className="polygon_white_register"
          fit="cover"
          src={polygon_white}
        />
        <Heading
          className="introTitle"
          alignSelf="center"
          size="small"
          color="white"
          margin="medium"
        >
          Start Reading Now
        </Heading>
        <Form>
          <FormField
            onChange={this.onNameChange.bind(this)}
            margin="medium"
            name="name"
            label="Full Name"
          />
          <FormField
            onChange={this.onEmailChange.bind(this)}
            margin="medium"
            name="email"
            label="Email"
          />
          <FormField
            onChange={this.onPWChange.bind(this)}
            margin="medium"
            type="password"
            name="password"
            label="Password"
          />

          <Button
            onClick={this.onSubmitClick.bind(this)}
            margin={"small"}
            color={"#6FFEB0"}
            type="submit"
            label="Submit"
          />
        </Form>
      </Box>
    );
  }
}

export default Register;
