import React, { Component } from "react";
import ArticlePreview from "./ArticlePreview.js";

class ArticleFeed extends Component {
  render() {
    return (
      <div>
        <ArticlePreview />
        <ArticlePreview />
        <ArticlePreview />
        <ArticlePreview />
        <ArticlePreview />
        <ArticlePreview />
        <ArticlePreview />
        <ArticlePreview />
      </div>
    );
  }
}

export default ArticleFeed;
