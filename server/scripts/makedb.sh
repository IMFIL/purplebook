#!/bin/bash

set -e # exit immediately on error
set -x # trace commands

psql postgresql://postgres:postgres@localhost/purplebook -c 'DROP TABLE IF EXISTS users CASCADE;DROP TABLE IF EXISTS sessions CASCADE;DROP TABLE IF EXISTS expired_sessions CASCADE;'
PGPASSWORD=postgres psql -f db/schema.sql postgresql://postgres:postgres@localhost/purplebook
